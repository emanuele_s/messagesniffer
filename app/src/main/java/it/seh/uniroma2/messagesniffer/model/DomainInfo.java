package it.seh.uniroma2.messagesniffer.model;

import it.seh.uniroma2.messagesniffer.constant.enumerations.ApplicationNameEnum;
import it.seh.uniroma2.messagesniffer.model.cache.Cacheable;

/**
 * Immutable class that extends <code>{@link Cacheable}</code>, so it can be use for caching
 * purpose. It represents the discovered information related to an IP address. Contains a name for
 * the domain and an <code>{@link ApplicationNameEnum}</code> related to the application associated
 * to the IP address.
 */
public class DomainInfo extends Cacheable {
    private String domainName;
    private ApplicationNameEnum application;

    /**
     * Public constructor using fields.
     * @param domainName
     * @param application
     */
    public DomainInfo( String domainName, ApplicationNameEnum application ){
        this.domainName = domainName;
        this.application = application;
    }

    /**
     * Getter method for domainName.
     * @return
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Getter method for application.
     * @return
     */
    public ApplicationNameEnum getApplication() {
        return application;
    }
}
