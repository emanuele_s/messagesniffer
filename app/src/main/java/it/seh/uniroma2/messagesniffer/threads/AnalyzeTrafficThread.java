package it.seh.uniroma2.messagesniffer.threads;

import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import java.util.List;

import it.seh.uniroma2.messagesniffer.constant.enumerations.ApplicationNameEnum;
import it.seh.uniroma2.messagesniffer.helpers.ByteArrayConvertHelper;
import it.seh.uniroma2.messagesniffer.model.AnalyzedPacket;
import it.seh.uniroma2.messagesniffer.model.DomainInfo;
import it.seh.uniroma2.messagesniffer.model.Packet;
import it.seh.uniroma2.messagesniffer.model.cache.AnalyzedPacketsCacheSingleton;
import it.seh.uniroma2.messagesniffer.sqlite.SQLIteHelper;

import static it.seh.uniroma2.messagesniffer.MainActivity.getContext;
import static it.seh.uniroma2.messagesniffer.constant.Constants.PARCELABLE_ANALYZED_PACKET_KEY;

/**
 * This class extends <code>{@link Thread}</code>. It is used to analyze intercepted packets
 * in order to find <code>{@link DomainInfo}</code> associated to the IP address. This is achieved using
 * information stored in a <code>{@link android.database.sqlite.SQLiteDatabase}</code> accessed
 * through <code>{@link SQLIteHelper}</code>.
 */
public class AnalyzeTrafficThread extends Thread {
	private List<Packet> sniffedPackets;
	private ResultReceiver resultReceiver;

	/**
	 * Public constructor get a <code>{@link List}</code> of intercepted <code>{@link Packet}</code>
	 * data and the <code>{@link ResultReceiver}</code> to which send the valid
	 * <code>{@link Packet}</code> with the application information.
	 *
	 * @param sniffedPackets
	 * @param resultReceiver
	 */
	public AnalyzeTrafficThread(List<Packet> sniffedPackets, ResultReceiver resultReceiver ){
		this.sniffedPackets = sniffedPackets;
		this.resultReceiver = resultReceiver;
	}

	/**
	 * Perform the AnalyzeTrafficThread's job. It calls
	 * <code>{@link AnalyzeTrafficThread#analyzePacket(Packet)}</code> for each
	 * <code>{@link Packet}</code> in <code>{@link AnalyzeTrafficThread#sniffedPackets}</code>.
	 */
	public void run(){
		if( sniffedPackets == null || resultReceiver == null ){
			Log.d( "AnalyzeTrafficThread", "invalid arguments in run : sniffedPackets -> "+sniffedPackets+" resultReceiver -> "+resultReceiver );
			this.interrupt();
		}
		for( Packet packet : sniffedPackets ){
			analyzePacket( packet );
		}
		sendResultFinish();
		interrupt();
	}

	/**
	 * Before searching the <code>{@link DomainInfo}</code> in the database it tries to find it in cache. If
	 * there is a cache miss the method <code>{@link AnalyzeTrafficThread#analyzePacket(Packet)}</code>
	 * is called.
	 * @param packet
	 */
	private void analyzePacket( Packet packet ){
		DomainInfo domainInfo = AnalyzedPacketsCacheSingleton.getInstance().findCachedDomainInfo( ByteArrayConvertHelper.convertByteArrayToIpAddress( packet.getDestinationAddress() ) );
		if( domainInfo == null ){
			domainInfo = findDomainFromIp( ByteArrayConvertHelper.convertByteArrayToIpAddress( packet.getDestinationAddress() ) );
		}
		sendResultPacket( domainInfo, packet );
	}

	/**
	 * Find domain information from ip address connecting to a
	 * <code>{@link android.database.sqlite.SQLiteDatabase}</code> using
	 * <code>{@link SQLIteHelper}</code>. If there is a match for the IP address the
	 * retrieved <code>{@link DomainInfo}</code> is putted in cache.
	 * @param ip
	 * @return
	 */
	private DomainInfo findDomainFromIp( String ip ){
		DomainInfo domainInfo = AnalyzedPacketsCacheSingleton.getInstance().findCachedDomainInfo( ip );
		if( domainInfo == null ) {
			SQLIteHelper sqlIteHelper = new SQLIteHelper(getContext());
			ApplicationNameEnum applicationNameEnum = sqlIteHelper.findAplicationName(ip);
			if (applicationNameEnum == null) {
				applicationNameEnum = ApplicationNameEnum.UNCATEGORIZED;
			}
			domainInfo = putInCache(  ip, applicationNameEnum.getApplicationName(), applicationNameEnum );
		}
		return domainInfo;
	}

	/**
	 * Put in cache <code>{@link DomainInfo}</code>.
	 * @param ip
	 * @param domainName
	 * @param appName
	 * @return
	 */
	private DomainInfo putInCache( String ip, String domainName, ApplicationNameEnum appName ){
		DomainInfo domainInfo = new DomainInfo( domainName, appName );
		AnalyzedPacketsCacheSingleton.getInstance().putDomainInfoInCache( ip, domainInfo );
		return domainInfo;
	}

	/**
	 * Send an <code>{@link AnalyzedPacket}</code> to the <code>{@link ResultReceiver}</code>.
	 * @param domainInfo
	 */
	private void sendResultPacket( DomainInfo domainInfo, Packet packet ){
		if( domainInfo == null ){
			return;
		}
		Bundle bundle = new Bundle();
		bundle.putParcelable( PARCELABLE_ANALYZED_PACKET_KEY, new AnalyzedPacket( domainInfo, packet ));
		resultReceiver.send( 0, bundle );
	}

	/**
	 * Send a message to the <code>{@link ResultReceiver}</code> in order to acknowledge the end of
     * the analyze task.
	 */
	private void  sendResultFinish(){
		resultReceiver.send( 1, new Bundle() );
	}
}



