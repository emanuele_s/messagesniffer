package it.seh.uniroma2.messagesniffer.helpers;

import android.util.Log;

import java.nio.ByteBuffer;

import it.seh.uniroma2.messagesniffer.model.Packet;

/**
 * Helper class used to parse the incoming data from the vpn interface
 *
 */

public class HandlePacketHelper{
    private HandlePacketHelper(){}

    /**
     * It takes a <code>{@link ByteBuffer}</code> that contains the bytes of an IP datagram and divides the
     * retrieve sensitive parts it is composed by in the following way:
     * <br>
     * <ul>
     *     <li>
     *         <bold>version</bold> - the first 4 bits are the version so it takes the first byte and
     *         shift it to the right in order to retrieve the 4 bits relative to the version.
     *     </li>
     *     <li>
     *         <bold>header length</bold> - needed to know when the data field begins. This is the
     *         second group of 4 bits of the first byte. So to retrieve it it is necessary to make
     *         a bitwise and operation of the first byte and the byte 0x0F (00001111) and then shift
     *         left the result, that is a multiplication by 4.
     *     </li>
     *     <li>
     *         <bold>data</bold> - in order to retrieve data bytes is necessary to retrieve the
     *         length of the TCP packet header and sum it to the above header length. It is given
     *         by the first 4 bit of the twelfth byte of the wrapped TCP packet. Data field is given
     *         by the rest of the packet one we exclude headers.
     *     </li>
     * </ul>
     * @param byteBuffer contains the bytes of a packet
     * @throws IllegalArgumentException if param byteBuffer is null
     * @return <code>{@link Packet}</code> containing the parsed information from the <code>{@link ByteBuffer}</code>
     */
    public static Packet parseByteBufferPacket( ByteBuffer byteBuffer ){
        ByteBuffer packetBB1 = byteBuffer.duplicate(), packetBB2 = byteBuffer.duplicate();

        if( byteBuffer == null ){
            Log.e( HandlePacketHelper.class.getName(), "parseByteBufferPacket: null argument -> byteBuffer." );
            throw new IllegalArgumentException( "parseByteBufferPacket: null argument -> byteBuffer." );
        }

        Packet packet = new Packet();

        byte tempData = byteBuffer.get();
        packet.setVersion( (byte)(tempData >> 4) );
        packet.setHeaderLength( (byte)((tempData & 0x0F)*4) );

        packet.setUpLayerProtocol( byteBuffer.get( 9 ) );

        packet.setSourceAddress( findSourceIpAdress( byteBuffer ) );
        packet.setDestinationAddress( findDestinationIpAdress( byteBuffer ) );

        packet.setSourcePort( findSourcePort( byteBuffer, packet.getHeaderLength() ) );
        packet.setDestinationPort( findDestinationPort( byteBuffer, packet.getHeaderLength() ) );

        int dataOffset = ((packetBB2.get(packet.getHeaderLength()+12))& 0xFF)>>>4;
        byte[] header = new byte[packet.getHeaderLength()+dataOffset];
        byte[] data =  new byte[packetBB1.limit()-packet.getHeaderLength()-dataOffset];
        packetBB2.get(header,0,packet.getHeaderLength()+dataOffset);
        packetBB2.get(data,0,packetBB1.limit()-packet.getHeaderLength()-dataOffset);
        packet.setHeader( header );
        packet.setData( ByteBuffer.wrap( data ) );


        return packet;

    }

    /**
     * Static method used to retrieve the 4 bytes of the source IP address
     * @param byteBuffer
     * @throws IllegalArgumentException if param byteBuffer is null
     * @return
     */
    public static byte[] findSourceIpAdress( ByteBuffer byteBuffer ){
        if( byteBuffer == null ){
            Log.e( HandlePacketHelper.class.getName(), "findSourceIpAdress: null argument -> byteBuffer." );
            throw new IllegalArgumentException( "findSourceIpAdress: null argument -> byteBuffer." );
        }
        byte[] address = new byte[4];
        address[0] = byteBuffer.get( 12 );
        address[1] = byteBuffer.get( 13 );
        address[2] = byteBuffer.get( 14 );
        address[3] = byteBuffer.get( 15 );

        return address;
    }

    /**
     * Static method used to retrieve the 4 bytes of the destination IP address
     * @param byteBuffer
     * @throws IllegalArgumentException if param byteBuffer is null
     * @return
     */
    public static byte[] findDestinationIpAdress( ByteBuffer byteBuffer ){
        if( byteBuffer == null ){
            Log.e( HandlePacketHelper.class.getName(), "findDestinationIpAdress: null argument -> byteBuffer." );
            throw new IllegalArgumentException( "findDestinationIpAdress: null argument -> byteBuffer." );
        }
        byte[] address = new byte[4];
        address[0] = byteBuffer.get( 16 );
        address[1] = byteBuffer.get( 17 );
        address[2] = byteBuffer.get( 18 );
        address[3] = byteBuffer.get( 19 );
        return address;
    }

    /**
     * Static method used to retrieve the 2 bytes of the source port
     * @param byteBuffer
     * @param headerLength
     * @throws IllegalArgumentException if param byteBuffer is null
     * @return
     */
    public static byte[] findSourcePort( ByteBuffer byteBuffer, int headerLength ){
        if( byteBuffer == null ){
            Log.e( HandlePacketHelper.class.getName(), "findSourcePort: null argument -> byteBuffer." );
            throw new IllegalArgumentException( "findSourcePort: null argument -> bytebuffer." );
        }
        byte[] port = new byte[2];
        port[0] = byteBuffer.get( headerLength );
        port[1] = byteBuffer.get( headerLength + 1 );


        return port;
    }

    /**
     * Static method used to retrieve the 2 bytes of the destination port
     * @param byteBuffer
     * @param headerLength
     * @throws IllegalArgumentException if param byteBuffer is null
     * @return
     */
    public static byte[] findDestinationPort( ByteBuffer byteBuffer, int headerLength ){
        if( byteBuffer == null ){
            Log.e( HandlePacketHelper.class.getName(), "findDestinationPort: null argument -> byteBuffer." );
            throw new IllegalArgumentException( "findDestinationPort: null argument -> bytebuffer." );
        }
        byte[] port = new byte[2];
        port[0] = byteBuffer.get( headerLength + 2 );
        port[1] = byteBuffer.get( headerLength + 3 );


        return port;
    }
}
