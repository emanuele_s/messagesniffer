package it.seh.uniroma2.messagesniffer.model.cache;

import android.util.Log;

import java.util.concurrent.ConcurrentHashMap;

import it.seh.uniroma2.messagesniffer.model.DomainInfo;

/**
 * Singleton class implementing a cache mechanism for retrieved information related to an IP address
 * and the <code>{@link DomainInfo}</code> associated with.
 */
public final class AnalyzedPacketsCacheSingleton {
	private final static AnalyzedPacketsCacheSingleton INSTANCE = new AnalyzedPacketsCacheSingleton();
	private final static ConcurrentHashMap<String, DomainInfo> cache = new ConcurrentHashMap<>();

	/**
	 * Private constructor.
	 */
	private AnalyzedPacketsCacheSingleton(){}

	/**
	 * Returns singleton INSTANCE.
	 * @return
     */
	public final static AnalyzedPacketsCacheSingleton getInstance(){
		return INSTANCE;
	}

	/**
	 * It throws <code>{@link IllegalArgumentException}</code> if ip is null.
	 * <p>Return a <code>{@link DomainInfo}</code> from cache if it is present and the validity time
	 * is not expired.</p>
	 * @param ip
	 * @return
     */
	public final DomainInfo findCachedDomainInfo(String ip ){
		if( ip == null ){
			Log.e( "AnalyzedPacketsCache", "findCachedDomainInfo : IllegaArgument -> ip = "+ip );
			throw new IllegalArgumentException( "findCachedDomainInfo : IllegaArgument -> ip = "+ip );
		}
		DomainInfo domainInfo = cache.get( ip );
		if( domainInfo == null || !domainInfo.isValid() ){
			cache.remove( ip );
			domainInfo = null;
		}
		return domainInfo;
	}

	/**
	 * Insert in cache a new entry of <code>{@link DomainInfo}</code>.
	 * @param ip
	 * @param domainInfo
     */
	public final void putDomainInfoInCache( String ip, DomainInfo domainInfo ){
		cache.put( ip, domainInfo );
	}
}



