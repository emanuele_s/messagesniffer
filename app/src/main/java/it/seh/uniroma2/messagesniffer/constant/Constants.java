package it.seh.uniroma2.messagesniffer.constant;

/**
 * Constant class containing the fields used to pass
 * <code>{@link it.seh.uniroma2.messagesniffer.model.Packet}</code> from thread to
 * <code>{@link android.os.ResultReceiver}</code>.
 */
public class Constants {
    /**
     * Private constructor.
     */
    private Constants(){}

    public final static String PARCELABLE_SNIFFED_PACKETS_KEY = "ParcelableSniffedPacketsKey";
    public final static String PARCELABLE_ANALYZED_PACKET_KEY = "ParcelableAnalyzedPacketKey";
}
