package it.seh.uniroma2.messagesniffer.constant.enumerations;

import android.util.Log;

import it.seh.uniroma2.messagesniffer.R;

/**
 * It is an enum related to the analyzed applications of which there are IP information loaded in
 * <code>{@link android.database.sqlite.SQLiteDatabase}</code>.
 * <p>Each entry has two information: a name (applicationName) and an integer related to the stored
 * application icon used by <code>{@link it.seh.uniroma2.messagesniffer.pdf.PdfConstructor}</code>
 * while drawing the PDF file.</p>
 */
public enum ApplicationNameEnum {
	WHATSAPP( "Whatsapp", R.raw.whatsapp ),
	TELEGRAM( "Telegram", R.raw.telegram ),
	WICKR( "Wickr", R.raw.wickr_icon ),
	LINE( "Line", R.raw.line ),
	SKYPE( "Skype", R.raw.skype ),
	SNAPCHAT( "Snapchat", R.raw.snapchat ),
	TANGO( "Tango", R.raw.tango ),
	THREEMA( "Threema", R.raw.threema ),
	VIBER( "Viber", R.raw.viber ),
	WECHAT( "WeChat", R.raw.wechat ),
	YAHOO( "Yahoo Messenger", R.raw.yahoo ),
	UNCATEGORIZED("Uncategorized Packets", R.raw.question_mark );

	private String applicationName;
	private int iconPath;

	/**
	 *
	 * @param applicationName
	 * @param iconPath
     */
	private ApplicationNameEnum( String applicationName, int iconPath ) {
		this.applicationName = applicationName;
		this.iconPath = iconPath;
	}

	/**
	 * Getter method for applicationName
	 * @return
     */
	public String getApplicationName(){
		return this.applicationName;
	}

	/**
	 * Getter method for iconPath
	 * @return
     */
	public int getIconPath() {
		return iconPath;
	}

	/**
	 * Returns the <code>{@link ApplicationNameEnum}</code> corresponding to the parameter
	 * applicationName.
	 * @param applicationName
	 * @return
     */
	public static ApplicationNameEnum fromApplicationName( String applicationName ){
		Log.d( "APPLICATION NAME -> ", applicationName );
		ApplicationNameEnum applicationNameEnum = null;
		if( applicationName != null ) {
			if (applicationName.equals(WHATSAPP.getApplicationName())) {
				applicationNameEnum = WHATSAPP;
			} else if (applicationName.equals(TELEGRAM.getApplicationName())) {
				applicationNameEnum = TELEGRAM;
			} else if (applicationName.equals(WICKR.getApplicationName())) {
				applicationNameEnum = WICKR;
			} else if (applicationName.equals(LINE.getApplicationName())) {
				applicationNameEnum = LINE;
			} else if (applicationName.equals(SKYPE.getApplicationName())) {
				applicationNameEnum = SKYPE;
			} else if (applicationName.equals(SNAPCHAT.getApplicationName())) {
				applicationNameEnum = SNAPCHAT;
			} else if (applicationName.equals(TANGO.getApplicationName())) {
				applicationNameEnum = TANGO;
			} else if (applicationName.equals(THREEMA.getApplicationName())) {
				applicationNameEnum = THREEMA;
			} else if (applicationName.equals(VIBER.getApplicationName())) {
				applicationNameEnum = VIBER;
			} else if (applicationName.equals(WECHAT.getApplicationName())) {
				applicationNameEnum = WECHAT;
			} else if (applicationName.equals(YAHOO.getApplicationName())) {
				applicationNameEnum = YAHOO;
			}
		}
		return applicationNameEnum;
	}
}
