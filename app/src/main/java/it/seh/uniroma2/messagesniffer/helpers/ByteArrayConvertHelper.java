package it.seh.uniroma2.messagesniffer.helpers;

import android.util.Log;

import java.nio.ByteBuffer;

/**
 * Helper class that contain methods used to convert byte array to readable information such as an
 * integer representing a port number or a string representing the dot format IP address.
 */
public class ByteArrayConvertHelper {
    private ByteArrayConvertHelper(){}

    /**
     * It returns an integer representation of the array parameter. It throws
     * <code>{@link IllegalArgumentException}</code> if array is null or its length is > 4.
     * <p>More interesting is the case of array.length = 2. In such a case the first byte is the
     * most significant so it is multiplied by 256 that is equal to a left shift of 8 bits. The
     * bitwise logic AND operation is necessary because Java represent byte in 2 complement and
     * the maximum int positive value is 127, the bitwise AND with 8 bits with value 1 is a trick
     * to convert to the real integer value. If removed the returned value may be incorrect or
     * negative</p>.
     *
     * @param array
     * @throws IllegalArgumentException
     * @return
     */
    public static int convertByteArrayToInt( byte[] array ){
        if( array == null ){
            Log.e( ByteArrayConvertHelper.class.getName(), "convertByteArrayToInt: null argument -> array." );
            throw new IllegalArgumentException( "convertByteArrayToInt: null argument -> array." );
        }else if( array.length > 4 ){
            Log.e( ByteArrayConvertHelper.class.getName(), "convertByteArrayToInt: illegal argument -> array.length ("+array.length+") not admitted." );
            throw new IllegalArgumentException( "convertByteArrayToInt: illegal argument -> array.length ("+array.length+") not admitted." );
        }
        int converted = 0;
        ByteBuffer bb = ByteBuffer.wrap( array );
        switch ( array.length ){
            case 2:
                converted = (int)( ( array[0] & 0xFF ) ) * 256 + ( array[1] & 0xFF );
                break;
            case 4:
                converted = bb.getInt();
                break;
        }
        return converted;
    }

    /**
     * It convert an array of 4 bytes in a <code>{@link String}</code> representing an IP address
     * with dotted notation.
     * <p>It throws <code>{@link IllegalArgumentException}</code> if array is null or its length is
     * not equal to 4.</p>
     * @param array
     * @throws IllegalArgumentException
     * @return
     */
    public static String convertByteArrayToIpAddress( byte[] array ){
        if( array == null ){
            Log.e( ByteArrayConvertHelper.class.getName(), "convertByteArrayToIpAddress: null argument -> array." );
            throw new IllegalArgumentException( "convertByteArrayToIpAddress: null argument -> array." );
        }else if( array.length != 4 ){
            Log.e( ByteArrayConvertHelper.class.getName(), "convertByteArrayToIpAddress: illegal argument -> array.length ("+array.length+") not admitted." );
            throw new IllegalArgumentException( "convertByteArrayToIpAddress: illegal argument -> array.length ("+array.length+") not admitted." );
        }
        StringBuilder sb = new StringBuilder();
        sb.append( array[0]&0xFF );
        sb.append( "." );
        sb.append( array[1]&0xFF );
        sb.append( "." );
        sb.append( array[2]&0xFF );
        sb.append( "." );
        sb.append( array[3]&0xFF );
        return sb.toString();
    }
}
