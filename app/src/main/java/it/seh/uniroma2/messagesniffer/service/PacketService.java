package it.seh.uniroma2.messagesniffer.service;

import android.app.Service;
import android.content.Intent;
import android.net.VpnService;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.util.Log;

import org.apache.commons.net.whois.WhoisClient;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import it.seh.uniroma2.messagesniffer.helpers.ByteArrayConvertHelper;
import it.seh.uniroma2.messagesniffer.helpers.HandlePacketHelper;
import it.seh.uniroma2.messagesniffer.model.Packet;
import static it.seh.uniroma2.messagesniffer.constant.Constants.*;

/**
 * Extends <code>{@link VpnService}</code> and implements <code>{@link Runnable}</code>.
 * <p>This class contain the packet sniffing logic.</p>
 */
public class PacketService extends VpnService implements Runnable{
    ResultReceiver resultReceiver;
    private final static List<String> LOCAL_IP_ADDRESSES = new ArrayList<String>();
    private static String INTERNAL_IP_ADDRESS = null;
    private static Thread vpt;

    static{
        initAddresses();
        if( INTERNAL_IP_ADDRESS == null ){
            throw new IllegalArgumentException( "Unable to set local ip addresses" );
        }
    }

    private static boolean continueRunning = true;

    public static void stopThreadRunning(){
        continueRunning = false;
    }

    /**
     * It is used to initialize the class member
     * <code>{@link PacketService#INTERNAL_IP_ADDRESS}</code> that will be used by
     * the <code>{@link android.net.VpnService.Builder}</code> to initialize the VPN.
     */
    public static void initAddresses() {
        try {
            for ( Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements() ; ) {
                NetworkInterface intf = en.nextElement();
                for( Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements() ; ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if ( !inetAddress.isLoopbackAddress() ) {
                        String ip = inetAddress.getHostAddress().toString();
                        LOCAL_IP_ADDRESSES.add( ip );
                        if( ip != null && ip.startsWith( "10" ) ){
                            Log.d( "PacketService", "initAddresses -> "+ip );
                            assignLocalIpAddress( ip );
                        }
                        Log.d( "PacketService", "initAddresses -> "+ip );
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
    }

    /**
     * It takes as input the local ip address and assign to
     * <code>{@link PacketService#INTERNAL_IP_ADDRESS}</code> a value equals to the input IP address
     * plus 1 mod 255.
     * @param ip
     */
    private static void assignLocalIpAddress( String ip ){
        String[] splitedIP = ip.split( "\\." );
        if( splitedIP.length != 4 ){
            Log.e( "PacketService", "assignLocalIpAddresses : Not valid ip address found" );
            throw new IllegalArgumentException( "Not valid ip address found" );
        }
        Integer lastValue = Integer.parseInt( splitedIP[splitedIP.length - 1] );
        StringBuilder sb = new StringBuilder();
        if( lastValue != null ){
            lastValue=(lastValue+1)%255;
        }
        for( int i = 0; i < splitedIP.length-1; i++ ){
            sb.append( splitedIP[i] );
            sb.append( "." );
        }
        sb.append( lastValue );
        INTERNAL_IP_ADDRESS = sb.toString();
    }

    private ParcelFileDescriptor mInterface;
    //a. Configure a builder for the interface.
    Builder builder = new Builder();

    /**
     * Public constructor.
     */
    public PacketService() {
    }

    /**
     * Called when the service is stopped, it interrupts the running <code>{@link Thread}</code>.
     */
    @Override
    public void onDestroy(){
        if( vpt != null )
            vpt.interrupt();
        super.onDestroy();
    }

    /**
     * Called when the <code>{@link PacketService}</code> is started it starts a new
     * <code>{@link Thread}</code> using the <code>{@link PacketService}</code>
     * <code>{@link Runnable}</code> implementation.
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    public int onStartCommand( Intent intent, int flags, int startId ){
        continueRunning = true;

        resultReceiver = intent.getParcelableExtra( "receiver" );

//        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
//        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
//        String localIP = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
//                (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
//
//        Log.d("WIFI", localIP);

        vpt = new Thread( this, "PacketService" );
        vpt.start();

        return Service.START_STICKY;
    }

    /**
     * Overridden method of the interface <code>{@link Runnable}</code>. It is the job executed when
     * the <code>{@link Thread}</code> is started by
     * <code>{@link PacketService#onStartCommand(Intent, int, int)}</code>.
     */
    public void run() {
        try {

            mInterface = builder.setSession("PacketService")
                    .addAddress(INTERNAL_IP_ADDRESS, 24)
                    .addRoute("0.0.0.0", 0).establish();

            FileInputStream in = new FileInputStream(mInterface.getFileDescriptor());
            ByteBuffer packet = ByteBuffer.allocate(32767);

            while ( continueRunning ) {
                int length = in.read(packet.array());
                if (length > 0) {
                    packet.limit(length);
                    Packet myPacket = HandlePacketHelper.parseByteBufferPacket( packet );
                    sendResult( myPacket );
                    packet.clear();
                }
            }

        } catch (Exception e) {
            Log.d("VPNSERVICE", "catch");
            e.printStackTrace();
        } finally {
            Log.d("VPNSERVICE", "finally");
            try {
                if (mInterface != null) {
                    mInterface.close();
                    mInterface = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        onDestroy();
    }

    /**
     * It is used to send a sniffed <code>{@link Packet}</code> to the
     * <code>{@link it.seh.uniroma2.messagesniffer.MainActivity.SniffedPacketsReceiver}</code>.
     * @param packet
     */
    private void sendResult( Packet packet ){
        Bundle bundle = new Bundle();
        bundle.putParcelable( PARCELABLE_SNIFFED_PACKETS_KEY, packet );
        resultReceiver.send( 0 , bundle );
    }

    /**
     * Called when the VPN is stopped it close the <code>{@link PacketService#mInterface}</code>
     * file descriptor used to read the <code>{@link Packet}</code>.
     */
    @Override
    public void onRevoke() {
        try {
            if (mInterface != null) {
                mInterface.close();
                mInterface = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onRevoke();
    }
}
