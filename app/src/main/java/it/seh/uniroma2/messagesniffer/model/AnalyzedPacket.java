package it.seh.uniroma2.messagesniffer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * This is a wrapper class for an analyzed <code>{@link Packet}</code>. It contains the analyzed
 * <code>{@link Packet}</code> (packet) and the related information represented by
 * <code>{@link DomainInfo}</code>.
 *
 * <p>It implements the Interface <code>{@link Parcelable}</code> because it is passed
 * as parcelable data from
 * <code>{@link it.seh.uniroma2.messagesniffer.threads.AnalyzeTrafficThread}</code> to
 * <code>{@link it.seh.uniroma2.messagesniffer.MainActivity.AnalyzedTrafficReceiver}</code></p>.
 */
public class AnalyzedPacket implements Parcelable{
    DomainInfo domainInfo;
    Packet packet;

    /**
     *Public constructor using fields.
     * @param domainInfo
     * @param packet
     */
    public AnalyzedPacket( DomainInfo domainInfo, Packet packet ) {
        this.domainInfo = domainInfo;
        this.packet = packet;
    }

    /**
     * Implementation of <code>{@link Parcelable}</code> interface.
     * @param in
     */
    protected AnalyzedPacket(Parcel in) {
        packet = in.readParcelable(Packet.class.getClassLoader());
    }

    /**
     * Implementation of <code>{@link Parcelable}</code> interface.
     */
    public static final Creator<AnalyzedPacket> CREATOR = new Creator<AnalyzedPacket>() {
        @Override
        public AnalyzedPacket createFromParcel(Parcel in) {
            return new AnalyzedPacket(in);
        }

        @Override
        public AnalyzedPacket[] newArray(int size) {
            return new AnalyzedPacket[size];
        }
    };

    /**
     * Getter method for packet.
     * @return
     */
    public Packet getPacket() {
        return this.packet;
    }

    /**
     * Getter method for domainInfo.
     * @return
     */
    public DomainInfo getDomainInfo() {
        return domainInfo;
    }

    /**
     * Implementation of <code>{@link Parcelable}</code> interface.
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Implementation of <code>{@link Parcelable}</code> interface.
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(packet, flags);
    }
}
