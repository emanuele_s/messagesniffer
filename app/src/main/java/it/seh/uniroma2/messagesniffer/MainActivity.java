package it.seh.uniroma2.messagesniffer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.seh.uniroma2.messagesniffer.model.AnalyzedPacket;
import it.seh.uniroma2.messagesniffer.model.Packet;
import it.seh.uniroma2.messagesniffer.pdf.PdfConstructor;
import it.seh.uniroma2.messagesniffer.service.PacketService;
import it.seh.uniroma2.messagesniffer.threads.AnalyzeTrafficThread;

import static it.seh.uniroma2.messagesniffer.constant.Constants.PARCELABLE_ANALYZED_PACKET_KEY;
import static it.seh.uniroma2.messagesniffer.constant.Constants.PARCELABLE_SNIFFED_PACKETS_KEY;

/**
 * The unique activity of the entire application. It is responsible to show the layout that contains
 * the button that permits the user to launch the <code>{@link PacketService}</code> in order to
 * capture outgoing packets and to generate the pdf report.
 * <p>It extends the class <code>{@link AppCompatActivity}</code></p>.
 */
public class MainActivity extends AppCompatActivity {
    private  boolean click = false;
    private static Context CONTEXT;
    Button startStopButton;
    ProgressBar pbLoading;
    TextView tvStopMessage;
    private String lastPdfPath = null;

    List<Packet> sniffed;
    List<AnalyzedPacket> analyzed;
    private static volatile int packetsReceived = 0;
    public static Context getContext(){
        return CONTEXT;
    }

    /**
     * Overridden method of the extended <code>{@link AppCompatActivity}</code>. It is the method
     * called when the activity starts. It is the handler of the application flow. It sets an
     * <code>{@link android.view.View.OnClickListener}</code> on the button that will handle the
     * capture packets start and stop.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CONTEXT = getApplicationContext();
        setContentView(R.layout.activity_main);
        pbLoading = (ProgressBar) findViewById( R.id.pbLoading );
        tvStopMessage = (TextView) findViewById( R.id.tvStopServiceMessage );


        startStopButton = (Button) findViewById( R.id.snsButton );


        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( click ){
                    tvStopMessage.setText( R.string.tvGeneratePdf );
                    startStopButton.setClickable( false );
                    stopVpnService();
                }else{
                    packetsReceived = 0;
                    tvStopMessage.setVisibility( TextView.VISIBLE );
                    tvStopMessage.setText( R.string.tvStopMessage );
                    pbLoading.setVisibility( ProgressBar.VISIBLE );
                    pbLoading.setIndeterminate( true );
                    startStopButton.setText( ""+packetsReceived );
                    startStopButton.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 75f );
                    startVpnService();
                }
                click = !click;
            }
        });
    }
    Intent i;

    /**
     * Private method that is used to start the <code>{@link PacketService}</code>.
     */
    private void startVpnService(){
        sniffed = new ArrayList<>();
        Intent i = PacketService.prepare( getApplicationContext() );
        Log.d( "MAIN", "Intent -> "+i );
        if( i != null ) {
            i.putExtra( "receiver", new SniffedPacketsReceiver( null ) );
            startActivityForResult( i, 0 );
        }else{
            i = new Intent( getApplicationContext(), PacketService.class );
            i.putExtra( "receiver", new SniffedPacketsReceiver( null ) );
            startService( i );
        }
    }

    /**
     * Private method that is used to stop the <code>{@link PacketService}</code>.
     */
    private void stopVpnService(){
        PacketService.stopThreadRunning();
        Intent i = PacketService.prepare( getApplicationContext() );
        if( i != null ){
            stopService( i );
        }else {
            stopService( new Intent( getApplicationContext(), PacketService.class ) );
        }
        new AnalyzeTrafficThread( sniffed, new AnalyzedTrafficReceiver( null ) ).start();
        sniffed = null;
    }

    /**
     * Private method that is used to generate the pdf report using
     * <code>{@link PdfConstructor}</code>. In order to gain access to memory it is necessary to ask
     * to the use for <code>{@link android.Manifest.permission#READ_EXTERNAL_STORAGE}</code> and
     * <code>{@link android.Manifest.permission#WRITE_EXTERNAL_STORAGE}</code> permissions.
     */
    private void generateReport(){
        boolean success = true;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions( new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 0 );
            }
            lastPdfPath = PdfConstructor.generatePdfReport( analyzed );
            analyzed = null;
        } catch (DocumentException e) {
            success = false;
            e.printStackTrace();
        } catch (IOException e) {
            success = false;
            e.printStackTrace();
        }
        tvStopMessage.post(new Runnable() {
            @Override
            public void run() {
                tvStopMessage.setVisibility( TextView.INVISIBLE );
            }
        });
        pbLoading.post(new Runnable() {
            @Override
            public void run() {
                pbLoading.setVisibility( ProgressBar.INVISIBLE );
            }
        });
        startStopButton.post(new Runnable() {
            @Override
            public void run() {
                startStopButton.setClickable( true );
                startStopButton.setText( R.string.snsButtonStart );
                startStopButton.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 15f );
            }
        });
        if( success ) {
            Snackbar.make( findViewById( R.id.content_main ), "PDF READY!", Snackbar.LENGTH_INDEFINITE ).setAction( R.string.openPDF, new View.OnClickListener() {
                @Override
                public void onClick( View view ) {
                    if ( lastPdfPath != null ) {
                        Intent intent = new Intent( Intent.ACTION_VIEW );
                        intent.setDataAndType( FileProvider.getUriForFile( getContext(), "messagesniffer.provider", new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOCUMENTS ), lastPdfPath ) ), "application/pdf");
//                    intent.setFlags( Intent.FLAG_ACTIVITY_NO_HISTORY );
                        intent.setFlags( Intent.FLAG_GRANT_READ_URI_PERMISSION );
                        startActivity( intent );
                    }
                }
            }).show();
        }else{
            Toast.makeText( getContext(), R.string.unableToGenReport, Toast.LENGTH_SHORT ).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if ( id == R.id.action_settings ) {
            return true;
        }
        return super.onOptionsItemSelected( item );
    }

    public void onDestroy(){
        super.onDestroy();
    }

    /**
     * It extends <code>{@link ResultReceiver}</code> and is used to handle the
     * <code>{@link AnalyzedPacket}</code> received from the
     * <code>{@link AnalyzeTrafficThread}</code>. It adds the analyzed packets in the
     * <code>{@link List}</code> <code>{@link MainActivity#analyzed}</code>.
     */
    class AnalyzedTrafficReceiver extends ResultReceiver {

        public AnalyzedTrafficReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if( analyzed == null ) {
                analyzed = new ArrayList<AnalyzedPacket>();
            }
            if( resultCode == 0 ){
                analyzed.add( (AnalyzedPacket) resultData.getParcelable( PARCELABLE_ANALYZED_PACKET_KEY ) );
            }else if( resultCode == 1 ){
                generateReport();
            }
        }
    }

    /**
     * It extends <code>{@link ResultReceiver}</code> and is used to handle the
     * <code>{@link Packet}</code> received from the <code>{@link PacketService}</code>.
     * It adds the received packets in the <code>{@link List}</code>
     * <code>{@link MainActivity#sniffed}</code>.
     */
    class SniffedPacketsReceiver extends ResultReceiver {

        public SniffedPacketsReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Packet message = (Packet) resultData.getParcelable( PARCELABLE_SNIFFED_PACKETS_KEY );

            if( message != null) {
                sniffed.add( message );
                startStopButton.post(new Runnable() {
                    @Override
                    public void run() {
                        startStopButton.setText( ""+(++packetsReceived) );
                    }
                });
            }

        }
    }
}