package it.seh.uniroma2.messagesniffer.constant.sqlite;

/**
 * Constant class that describe the unique table created and used by
 * <code>{@link android.database.sqlite.SQLiteDatabase}</code>.
 */
public class SQLIteConstants {
    /**
     * Private constructor.
     */
    private SQLIteConstants(){};

    /**
     * The name of the database.
     */
    public final static String DATABASE_NAME = "MESSAGE_SNIFFER_DB";
    /**
     * Database version.
     */
    public final static int DATABASE_VERSION = 1;

    /**
     * The name of the table storing IP and AppName information.
     */
    public final static String TABLE_IP_NAME = "IP_ADDRESS";

    /**
     * IP column name.
     */
    public final static String COLUMN_IP_NAME = "IP";

    /**
     * Application name column name.
     */
    public final static String COLUMN_APP_NAME = "APPLICATION";
}
