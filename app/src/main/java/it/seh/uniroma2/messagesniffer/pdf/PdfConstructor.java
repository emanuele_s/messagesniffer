package it.seh.uniroma2.messagesniffer.pdf;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.seh.uniroma2.messagesniffer.MainActivity;
import it.seh.uniroma2.messagesniffer.R;
import it.seh.uniroma2.messagesniffer.constant.enumerations.ApplicationNameEnum;
import it.seh.uniroma2.messagesniffer.model.AnalyzedPacket;
import it.seh.uniroma2.messagesniffer.model.Packet;

/**
 * This class use iText library to generate a pdf report containing all the information gathered in
 * capturing <code>{@link Packet}</code> phase by
 * <code>{@link it.seh.uniroma2.messagesniffer.service.PacketService}</code>.
 */
public class PdfConstructor {

    /**
     * Static method used to generate a pdf report containing <code>{@link AnalyzedPacket}</code>
     * information. The name of the report is composed by the <code>{@link String}</code> 'report-'
     * and the actual <code>{@link LocalDateTime}</code> 'yyyyMMddhhmmss' formatted.
     *
     * @param packetsList is the list of the <code>{@link AnalyzedPacket}</code> that should be
     *                    written in the report.
     * @return String that represents the name of the generated report.
     * @throws DocumentException
     * @throws IOException
     */
    public static String generatePdfReport(List<AnalyzedPacket> packetsList) throws DocumentException, IOException {

        Map<ApplicationNameEnum, List<Packet>> packets = convertToMap(packetsList);
        String fileName = "report-" + getTodayDate() + ".pdf";
        Document doc = new Document();

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), fileName);

        file.createNewFile();
        PdfWriter a = PdfWriter.getInstance(doc, new FileOutputStream(file));
        doc.open();
        drawFirstPage(doc);
        int chapter = 0;

        for (Map.Entry<ApplicationNameEnum, List<Packet>> packetEntry : packets.entrySet()) {
            drawApplicationHeader(doc, new Chapter(++chapter), packetEntry.getKey());
            for (Packet packet : packetEntry.getValue()) {
                drawPacket(doc, packet);
            }
        }
        doc.close();
        return fileName;
    }

    /**
     * Static private support method that converts the <code>{@link List}</code> of
     * <code>{@link AnalyzedPacket}</code> in input to a <code>{@link Map}</code> with
     * <code>{@link ApplicationNameEnum}</code> as key and a <code>{@link List}</code> of
     * <code>{@link Packet}</code> as value.
     *
     * @param analyzedPackets
     * @return
     */
    private static Map<ApplicationNameEnum, List<Packet>> convertToMap(List<AnalyzedPacket> analyzedPackets) {
        Map<ApplicationNameEnum, List<Packet>> converted = new HashMap<>();
        if (analyzedPackets != null) {
            for (AnalyzedPacket ap : analyzedPackets) {
                List<Packet> packets = converted.get(ap.getDomainInfo().getApplication());
                if (packets == null) {
                    packets = new ArrayList<>();
                }
                packets.add(ap.getPacket());
                converted.put(ap.getDomainInfo().getApplication(), packets);
            }
        }
        return converted;
    }

    /**
     * Private static support method used to draw a black line on the <code>{@link Document}</code>.
     *
     * @param doc
     * @throws DocumentException
     */
    private static void drawLineBreak(Document doc) throws DocumentException {
        Chunk linebreak = new Chunk(new LineSeparator());
        doc.add(linebreak);
    }

    /**
     * Private static support method that draw the <code>{@link Packet}</code> in input on the input
     * <code>{@link Document}</code>. It use the <code>{@link Packet#toString()}</code> method to
     * draw the <code>{@link Packet}</code> itself. Then it write the
     * <code>{@link Packet#data}</code> using the <code>{@link StringBuilder}</code>.
     *
     * @param doc
     * @param packet
     * @throws DocumentException
     */
    private static void drawPacket(Document doc, Packet packet) throws DocumentException {
        doc.add(buildParagraph(packet.toString(), PdfHelperConstants.CHAPTER_FONT, Paragraph.ALIGN_JUSTIFIED, 3f));
        PdfPTable table = new PdfPTable(new float[]{1f});
        table.setWidthPercentage(100);
        PdfPCell line = new PdfPCell();
        line.setBorder(PdfPCell.NO_BORDER);
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("\n\nData Payload:\n");
            int lineLength = 30;
            for (int i = 0, o = lineLength; i < packet.getData().array().length; i += lineLength, o += lineLength) {
                if (o <= packet.getData().array().length) {
                    sb.append(new String(Arrays.copyOfRange(packet.getData().array(), i, o), "UTF-8") + "\n");
                } else {
                    sb.append(new String(Arrays.copyOfRange(packet.getData().array(), i, packet.getData().array().length), "UTF-8") + "\n");
                }
            }
            sb.append(new String(packet.getData().array(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.e("PACKET", "unsupported encoding");
            sb.append("unsupported encoding");
        }
        line.addElement(buildParagraph(sb.toString(), PdfHelperConstants.CHAPTER_FONT, Paragraph.ALIGN_JUSTIFIED, 0f));
        table.addCell(line);
        doc.add(table);
        drawLineBreak(doc);
    }

    /**
     * Private static support method used to draw a black line on the <code>{@link Chapter}</code>.
     *
     * @param chapter
     * @throws DocumentException
     */
    private static void drawLineBreak(Chapter chapter) throws DocumentException {
        Chunk linebreak = new Chunk(new LineSeparator());
        chapter.add(linebreak);
    }

    /**
     * Private static support method used to create a <code>{@link Paragraph}</code>.
     *
     * @param text
     * @param font
     * @param alignment
     * @param paddingTop
     * @return
     */
    private static Paragraph buildParagraph(String text, Font font, int alignment, float paddingTop) {
        Paragraph paragraph = new Paragraph(new Chunk(text, font));
        paragraph.setAlignment(alignment);
        paragraph.setPaddingTop(paddingTop);
        return paragraph;
    }

    /**
     * Private static support method used to create an <code>{@link Image}</code>.
     *
     * @param imageRes
     * @param alignment
     * @param scaleW
     * @param scaleH
     * @return
     * @throws BadElementException
     * @throws MalformedURLException
     * @throws IOException
     */
    private static Image buildImage(int imageRes, int alignment, float scaleW, float scaleH) throws BadElementException, MalformedURLException, IOException {
        InputStream ims = MainActivity.getContext().getResources().openRawResource(imageRes);
        Bitmap bmp = BitmapFactory.decodeStream(ims);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = Image.getInstance(stream.toByteArray());
        image.scaleToFit(scaleW, scaleH);
        image.setAlignment(alignment);
        return image;
    }

    /**
     * Private static support method used to draw the first page of the report. It draws the
     * university logo, the title, the author name and other information. These information are
     * taken from the inner class <code>{@link PdfHelperConstants}</code>.
     *
     * @param doc
     * @throws DocumentException
     * @throws MalformedURLException
     * @throws IOException
     */
    private static void drawFirstPage(Document doc) throws DocumentException, MalformedURLException, IOException {
        doc.add(buildImage(PdfHelperConstants.UNIVERSITY_LOGO_RES, Image.ALIGN_CENTER, 80, 80));
        doc.add(buildParagraph(PdfHelperConstants.DOCUMENT_TITLE, PdfHelperConstants.HEADER_FONT, Paragraph.ALIGN_CENTER, 5f));
        doc.add(buildParagraph(PdfHelperConstants.TITLE_1, PdfHelperConstants.HEADER_FONT, Paragraph.ALIGN_CENTER, 5f));
        doc.add(buildParagraph(PdfHelperConstants.TITLE_2, PdfHelperConstants.HEADER_FONT, Paragraph.ALIGN_CENTER, 0f));
        doc.add(buildParagraph(PdfHelperConstants.TITLE_3, PdfHelperConstants.HEADER_FONT, Paragraph.ALIGN_CENTER, 0f));
        doc.add(buildParagraph(PdfHelperConstants.COURSE_NAME, PdfHelperConstants.HEADER_FONT, Paragraph.ALIGN_CENTER, 0f));
        doc.add(buildParagraph(PdfHelperConstants.MY_NAME, PdfHelperConstants.HEADER_FONT, Paragraph.ALIGN_CENTER, 0f));
        doc.add(buildParagraph(PdfHelperConstants.ACADEMIC_YEAR, PdfHelperConstants.HEADER_FONT, Paragraph.ALIGN_CENTER, 0f));
    }

    /**
     * Private static support method that draws first lines of a new <code>{@link Chapter}</code>.
     * In particular for each <code>{@link ApplicationNameEnum}</code> in the
     * converted <code>{@link List}</code> of <code>{@link Packet}</code> by the method
     * <code>{{@link #convertToMap(List)}}</code> it draws a <code>{@link Chapter}</code> header
     * that contains an icon taken from <code>{@link ApplicationNameEnum#getIconPath()}</code> and
     * an application description taken from
     * <code>{@link ApplicationNameEnum#getApplicationName()}</code>.
     *
     * @param doc
     * @param chapter
     * @param appName
     * @throws DocumentException
     * @throws MalformedURLException
     * @throws IOException
     */
    private static void drawApplicationHeader(Document doc, Chapter chapter, ApplicationNameEnum appName) throws DocumentException, MalformedURLException, IOException {
        chapter.setNumberDepth(0);

        drawLineBreak(chapter);

        PdfPTable table = new PdfPTable(new float[]{1f, 1f});
        table.setWidthPercentage(100);

        PdfPCell imageLogo = new PdfPCell();
        imageLogo.setBorder(PdfPCell.NO_BORDER);
        imageLogo.addElement(buildImage(appName.getIconPath(), Image.ALIGN_LEFT, 30, 30));
        table.addCell(imageLogo);

        PdfPCell headerTitle = new PdfPCell();
        headerTitle.setBorder(PdfPCell.NO_BORDER);
        Phrase title = new Phrase(appName.getApplicationName());
        title.setFont(PdfHelperConstants.CHAPTER_HEADER_FONT);
        headerTitle.addElement(title);
        table.addCell(headerTitle);


        chapter.add(table);

        drawLineBreak(chapter);
        doc.add(chapter);
    }

    /**
     * Private static support method that returns a <code>{@link String}</code> containing the
     * actual <code>{@link LocalDateTime}</code> 'yyyyMMddhhmmss' formatted.
     *
     * @return
     */
    private static String getTodayDate() {
        return LocalDateTime.now().toString(DateTimeFormat.forPattern("yyyyMMddhhmmss"));
    }

    /**
     * Constant class.
     */
    private final static class PdfHelperConstants {
        /**
         * Private constructor.
         */
        private PdfHelperConstants() {
        }

        public final static String DOCUMENT_TITLE = "MessageSniffer Incoming Traffic Report";
        public final static String TITLE_1 = "Università degli Studi di Roma";
        public final static String TITLE_2 = "Tor Vergata";
        public final static String TITLE_3 = "Ingegneria Informatica";
        public final static String COURSE_NAME = "Sicurezza Informatica";
        public final static String MY_NAME = "Emanuele Santangelo";
        public final static String ACADEMIC_YEAR = "Anno Accademico 2015/2016";
        public final static int UNIVERSITY_LOGO_RES = R.raw.tvlogo;
        public final static float HEADER_FONT_SIZE = 22f;
        public final static Font HEADER_FONT = FontFactory.getFont(FontFactory.COURIER, HEADER_FONT_SIZE, Font.BOLD);
        public final static Font CHAPTER_HEADER_FONT = FontFactory.getFont(FontFactory.COURIER, HEADER_FONT_SIZE - 2, Font.BOLD);
        public final static Font CHAPTER_FONT = FontFactory.getFont(FontFactory.COURIER, HEADER_FONT_SIZE - 8, Font.NORMAL);
    }
}


