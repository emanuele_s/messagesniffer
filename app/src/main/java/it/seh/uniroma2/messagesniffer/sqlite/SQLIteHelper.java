package it.seh.uniroma2.messagesniffer.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.apache.commons.net.util.SubnetUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;

import it.seh.uniroma2.messagesniffer.R;
import it.seh.uniroma2.messagesniffer.constant.enumerations.ApplicationNameEnum;
import it.seh.uniroma2.messagesniffer.constant.sqlite.SQLIteConstants;

/**
 * It is an helper class used to abstract the operations that involve the
 * <code>{@link SQLiteDatabase}</code>.
 * It extends <code>{@link SQLiteOpenHelper}</code> the first time it is responsible to the
 * database creation.
 */
public class SQLIteHelper extends SQLiteOpenHelper {

    Context context;

    /**
     * Public constructor that get as input parameter the application <code>{@link Context}</code>
     * necessary to operate with the database.
     * @param context
     */
    public SQLIteHelper( Context context ) {
        super( context, SQLIteConstants.DATABASE_NAME, null, SQLIteConstants.DATABASE_VERSION );
        this.context = context;
    }

    /**
     * Overridden method, it is used to create the database schema. Once created the schema is
     * initialized calling <code>{@link SQLIteHelper#parseXmlDoc(SQLiteDatabase)}</code>.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder createTableQuery = new StringBuilder();
        createTableQuery.append( "CREATE TABLE " );
        createTableQuery.append( SQLIteConstants.TABLE_IP_NAME );
        createTableQuery.append( " ( " );
        createTableQuery.append( SQLIteConstants.COLUMN_IP_NAME );
        createTableQuery.append( " TEXT PRIMARY KEY, " );
        createTableQuery.append( SQLIteConstants.COLUMN_APP_NAME );
        createTableQuery.append( " TEXT )" );
        db.execSQL( createTableQuery.toString() );
        parseXmlDoc( db );
    }

    /**
     * This method is used to initialize the database. It uses <code>{@link XmlPullParser}</code> to
     * parse the xml file that contains the information related to the Application's ip addresses
     * and insert in the database these information.
     * @param db
     */
    private void parseXmlDoc(SQLiteDatabase db){
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            parser.setInput( context.getResources().openRawResource( R.raw.applications_server_ip ), null );

            int event = parser.getEventType();

            ApplicationNameEnum applicationName = null;

            while(event != parser.END_DOCUMENT){
                switch (event){
                    case XmlPullParser.START_TAG:
                        if( parser.getName().equals( "name" ) ){
                            applicationName = ApplicationNameEnum.fromApplicationName( parser.nextText() );
                        }else if(parser.getName().equals("ip")){
                            addIpAddressEntry( db, parser.nextText(), applicationName );
                        }
                        event = parser.next();
                        break;
                    case XmlPullParser.END_TAG:
                        event = parser.next();
                        break;
                    case XmlPullParser.TEXT:
                        event = parser.next();
                        break;
                    default:
                        event = parser.next();
                        break;
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Using <code>{@link SubnetUtils}</code> obtains the entire list of ip addresses related to a
     * subnet.
     * @param db
     * @param subnet
     * @param appName
     */
    public void addIpAddressEntry( SQLiteDatabase db, String subnet, ApplicationNameEnum appName ){
            if( subnet == null || appName == null ){
            throw new IllegalArgumentException( "addIpAddressEntry null arguments" );
        }

        String[] ips = null;
        String[] split = subnet.split( "/" );

        if( split != null && split.length == 2 && !split[1].equals( "32" ) ){
            SubnetUtils subnetUtils = new SubnetUtils( subnet );
            ips = subnetUtils.getInfo().getAllAddresses();
        }else{
            ips = new String[]{ split[0] };
        }

        for( String ip : ips ) {
            ContentValues cv = new ContentValues();
            cv.put(SQLIteConstants.COLUMN_IP_NAME, ip);
            cv.put(SQLIteConstants.COLUMN_APP_NAME, appName.getApplicationName());
            db.insertWithOnConflict( SQLIteConstants.TABLE_IP_NAME, null, cv, SQLiteDatabase.CONFLICT_IGNORE );
        }
    }

    /**
     * It is used to retrieve the <code>{@link ApplicationNameEnum}</code> related to an IP address.
     * @param ip
     * @return
     */
    public ApplicationNameEnum findAplicationName( String ip ){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query( SQLIteConstants.TABLE_IP_NAME, new String[] {SQLIteConstants.COLUMN_APP_NAME}, SQLIteConstants.COLUMN_IP_NAME+"=?", new String[]{ip}, null, null, null );
        ApplicationNameEnum applicationNameEnum = null;
        if( c.moveToFirst() && c.getString( c.getColumnIndex( SQLIteConstants.COLUMN_APP_NAME ) ) != null ){
            applicationNameEnum = ApplicationNameEnum.fromApplicationName( c.getString( c.getColumnIndex( SQLIteConstants.COLUMN_APP_NAME ) ) );
        }
        c.close();
        return applicationNameEnum;
    }
}
