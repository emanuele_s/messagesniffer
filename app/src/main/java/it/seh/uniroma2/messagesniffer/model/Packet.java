package it.seh.uniroma2.messagesniffer.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

/**
 * It contains the sensitive information of an IP Packet. It implements
 * <code>{@link Parcelable}</code> because it is passed as parcelable data from
 * <code>{@link it.seh.uniroma2.messagesniffer.service.PacketService}</code> to
 * <code>{@link it.seh.uniroma2.messagesniffer.MainActivity.SniffedPacketsReceiver}</code>.
 *
 */
public class Packet implements Parcelable{
    private byte version;
    private byte headerLength;
    private byte upLayerProtocol;
    private byte[] sourceAddress = new byte[4];
    private byte[] destinationAddress = new byte[4];
    private byte[] sourcePort = new byte[2];
    private byte[] destinationPort = new byte[2];
    private byte[] header = new byte[2];
    private ByteBuffer data;

    /**
     * Public constructor.
     */
    public Packet(){
        super();
    }

    /**
     * Implementation of <code>{@link Parcelable}</code> interface.
     * @param in
     */
    protected Packet(Parcel in) {
        version = in.readByte();
        headerLength = in.readByte();
        upLayerProtocol = in.readByte();
        sourceAddress = in.createByteArray();
        destinationAddress = in.createByteArray();
        sourcePort = in.createByteArray();
        destinationPort = in.createByteArray();
        header = in.createByteArray();
    }

    /**
     * Implementation of <code>{@link Parcelable}</code> interface.
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(version);
        dest.writeByte(headerLength);
        dest.writeByte(upLayerProtocol);
        dest.writeByteArray(sourceAddress);
        dest.writeByteArray(destinationAddress);
        dest.writeByteArray(sourcePort);
        dest.writeByteArray(destinationPort);
        dest.writeByteArray(header);
    }

    /**
     * Implementation of <code>{@link Parcelable}</code> interface.
     * @return
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Implementation of <code>{@link Parcelable}</code> interface.
     */
    public static final Creator<Packet> CREATOR = new Creator<Packet>() {
        @Override
        public Packet createFromParcel(Parcel in) {
            return new Packet(in);
        }

        @Override
        public Packet[] newArray(int size) {
            return new Packet[size];
        }
    };

    /**
     * Getter method for IP version.
     * @return
     */
    public byte getVersion() {
        return version;
    }

    /**
     * Setter method for version.
     * @param version
     */
    public void setVersion(byte version) {
        this.version = version;
    }

    /**
     * Getter method for headerLength.
     * @return
     */
    public byte getHeaderLength() {
        return headerLength;
    }

    /**
     * Setter method for headerLength.
     * @param headerLength
     */
    public void setHeaderLength(byte headerLength) {
        this.headerLength = headerLength;
    }

    /**
     * Getter method for sourceAddress.
     * @return
     */
    public byte[] getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Setter method for sourceAddress.
     * @param sourceAddress
     */
    public void setSourceAddress(byte[] sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    /**
     * Getter method for destinationAddress.
     * @return
     */
    public byte[] getDestinationAddress() {
        return destinationAddress;
    }

    /**
     * Setter method for destinationAddress.
     * @param destinationAddress
     */
    public void setDestinationAddress(byte[] destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    /**
     * Getter method for sourcePort.
     * @return
     */
    public byte[] getSourcePort() {
        return sourcePort;
    }

    /**
     * Setter method for sourcePort.
     * @param sourcePort
     */
    public void setSourcePort(byte[] sourcePort) {
        this.sourcePort = sourcePort;
    }

    /**
     * Getter method for destinationPort
     * @return
     */
    public byte[] getDestinationPort() {
        return destinationPort;
    }

    /**
     * Setter method for destinationPort
     * @param destinationPort
     */
    public void setDestinationPort(byte[] destinationPort) {
        this.destinationPort = destinationPort;
    }

    /**
     * Getter method for upLayerProtocol
     * @return
     */
    public byte getUpLayerProtocol() {
        return upLayerProtocol;
    }

    /**
     * Setter method for upLayerProtocol
     * @param upLayerProtocol
     */
    public void setUpLayerProtocol(byte upLayerProtocol) {
        this.upLayerProtocol = upLayerProtocol;
    }

    /**
     * Getter method for header
     * @return
     */
    public byte[] getHeader() {
        return header;
    }

    /**
     * Setter method for header
     * @param header
     */
    public void setHeader(byte[] header) {
        this.header = header;
    }

    /**
     * Getter method for data
     * @return
     */
    public ByteBuffer getData() {
        return data;
    }

    /**
     * Setter method for data
     * @param data
     */
    public void setData(ByteBuffer data) {
        this.data = data;
    }


    /**
     * String representation of the <code>{@link Packet}</code>. This is used to write the
     * <code>{@link Packet}</code> to the PDF.
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append( "Version................... " );
        sb.append( ""+version );
        sb.append( "\n" );
        sb.append( "Source Address............ " );
        sb.append( (sourceAddress[0]&0xFF) +"."+(sourceAddress[1]&0xFF)  +"."+(sourceAddress[2]&0xFF)  +"."+(sourceAddress[3]&0xFF) );
        sb.append( "\n" );
        sb.append( "Destination Address....... " );
        sb.append( (destinationAddress[0]&0xFF) +"."+(destinationAddress[1]&0xFF)  +"."+(destinationAddress[2]&0xFF)  +"."+(destinationAddress[3]&0xFF) );
        sb.append( "\n" );
        sb.append( "Source Port............... " );
        sb.append( ((int)( ( sourcePort[0] & 0xFF ) ) * 256 + ( sourcePort[1] & 0xFF )) );
        sb.append( "\n" );
        sb.append( "Destination Port.......... " );
        sb.append( ((int)( ( destinationPort[0] & 0xFF ) ) * 256 + ( destinationPort[1] & 0xFF )) );
        sb.append( "\n" );
        sb.append( "Data Payload Length....... " );
        sb.append( data.array().length );
        sb.append( "\n" );
        return sb.toString();
    }
}
